##
## Makefile for firewall
##
## Made by jerome schneider Entr'ouvert
## Login   <jschneider@entrouvert.org>
##

NAME=eofirewall
VERSION=`git describe`
DESTDIR=

all:

install:
		install -d -m 0755 -o root -g root $(DESTDIR)/etc/rsyslog.d $(DESTDIR)/usr/bin
		install -d -m 0755 -o root -g root $(DESTDIR)/etc/firewall
		install    -m 0640 -o root -g root firewall.conf $(DESTDIR)/etc/firewall/firewall.conf.template
		install    -m 0640 -o root -g root rsyslog.conf $(DESTDIR)/etc/rsyslog.d/eofirewall.conf
		install    -m 0755 -o root -g root $(NAME) $(DESTDIR)/usr/bin
clean:
	rm -rf build sdist

dist-bzip2: clean
	mkdir -p build/$(NAME)-$(VERSION)
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done
	mkdir sdist
	cd build && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 .

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))

